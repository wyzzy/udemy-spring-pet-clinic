package guru.springframework.udemypetclinic.services;

import guru.springframework.udemypetclinic.model.Pet;

import java.util.Set;

public interface PetService {
    Pet findById(Long id);
    Pet save(Pet pet);
    Set<Pet> findAll();
}
